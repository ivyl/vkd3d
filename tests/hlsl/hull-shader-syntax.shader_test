[require]
shader model >= 5.0

%% Test patch constant function overloads.
%% For SM < 6, the last declaration that is defined is picked.
%% For SM >= 6, the last valid declaration is picked, even if it is not defined.

[hull shader]
struct patch_constant_data
{
    float edges[2] : SV_TessFactor;
};

patch_constant_data patch_constant(float x);
patch_constant_data patch_constant(float x, uint y);

patch_constant_data patch_constant()
{
    return (patch_constant_data)0;
}

// Missing valid return semantics (SV_TessFactor).
float patch_constant(uint x);

    [domain("isoline")]
    [outputcontrolpoints(3)]
    [partitioning("integer")]
    [outputtopology("point")]
    [patchconstantfunc("patch_constant")]
float4 main() : POSITION
{
    return 0;
}

[hull shader fail(sm>=6)]
struct patch_constant_data
{
    float edges[2] : SV_TessFactor;
};

patch_constant_data patch_constant(float x);
patch_constant_data patch_constant(float x, uint y);

patch_constant_data patch_constant()
{
    return (patch_constant_data)0;
}

patch_constant_data patch_constant(uint x : SV_PrimitiveID);

    [domain("isoline")]
    [outputcontrolpoints(3)]
    [partitioning("integer")]
    [outputtopology("point")]
    [patchconstantfunc("patch_constant")]
float4 main() : POSITION
{
    return 0;
}

[hull shader]
struct patch_constant_data
{
    float edges[2] : SV_TessFactor;
};

patch_constant_data patch_constant()
{
    return (patch_constant_data)0;
}

patch_constant_data patch_constant(uint x : SV_PrimitiveID)
{
    return (patch_constant_data)x;
}

    [domain("isoline")]
    [outputcontrolpoints(3)]
    [partitioning("integer")]
    [outputtopology("point")]
    [patchconstantfunc("patch_constant")]
float4 main() : POSITION
{
    return 0;
}

[hull shader fail todo]
struct patch_constant_data
{
    float edges[2] : SV_TessFactor;
};

patch_constant_data patch_constant()
{
    return (patch_constant_data)0;
}

// x is missing a semantic.
patch_constant_data patch_constant(uint x)
{
    return (patch_constant_data)x;
}

    [domain("isoline")]
    [outputcontrolpoints(3)]
    [partitioning("integer")]
    [outputtopology("point")]
    [patchconstantfunc("patch_constant")]
float4 main() : POSITION
{
    return 0;
}
